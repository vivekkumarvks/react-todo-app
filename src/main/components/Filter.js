import React, { Component } from "react";
import "./component.css";

class Filter extends Component {
  render() {
    return (
      <div className="block filter">
        <button
          className="all-button filter-button"
          onClick={() => this.props.filter("all")}
          type="button"
        >
          All
        </button>
        <button
          className="filter-button"
          onClick={() => this.props.filter("completed")}
        >
          Completed
        </button>
        <button
          className="filter-button"
          onClick={() => this.props.filter("uncompleted")}
        >
          UnCompleted
        </button>
      </div>
    );
  }
}

export default Filter;
